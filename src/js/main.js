/*
 * main.js - Website main logic
 *
 * Copyright 2013 - 2023 Felipe Peñailillo <fcpc.1984@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see https://www.gnu.org/licenses/.
*
*/

// Custom $(document).ready() function
function ready(fn) {
    if (document.readyState != "loading") {
        fn();
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

// Callback function that updates langstrings
function updateLangstrings() {
    // Gets all elements that have data-langstring attribute
    const langstrings = document.querySelectorAll('[data-langstring]');
    // Loops through langstrings array, applies translations using its data-langstring value as key
    [...langstrings].map(element => element.innerHTML = i18next.t(element.dataset.langstring));
}

ready(() => {
    // Dark Mode init
    const navButtons = document.querySelectorAll('nav button');
    if (localStorage.getItem("dark-mode") !== null && localStorage.getItem("dark-mode") == "false") {
        document.documentElement.dataset.bsTheme = "light";
        document.getElementById("mode-toggler").innerHTML = '<i class="fa-solid fa-sun"></i>';
        [...navButtons].map(element => element.classList.replace("btn-outline-light", "btn-outline-dark"));
    }
    document.getElementById("mode-toggler").addEventListener("click", () => {
        let darkMode = localStorage.getItem("dark-mode") === null ? true : Boolean(localStorage.getItem("dark-mode") == "true");
        localStorage.setItem("dark-mode", !darkMode);
        if (JSON.parse(localStorage.getItem("dark-mode"))) {
            document.documentElement.dataset.bsTheme = "dark";
            document.getElementById("mode-toggler").innerHTML = '<i class="fa-solid fa-moon"></i>';
        } else {
            document.documentElement.dataset.bsTheme = "light";
            document.getElementById("mode-toggler").innerHTML = '<i class="fa-solid fa-sun"></i>';
        }
        // Loops through navbuttons array, toggles its classes
        [...navButtons].map(element => {element.classList.toggle("btn-outline-light"); element.classList.toggle("btn-outline-dark");});
    });
    // i18next init
    i18next.use(i18nextBrowserLanguageDetector).init({
        fallbackLng: 'en',
        resources: {
            en: {
                translation: {
                    "language": "EN",
                    "under_construction": "Under Construction",
                    "contact_part1": "Need to contact me?",
                    "contact_part2": "You can do it via"
                }
            },
            es: {
                translation: {
                    "language": "ES",
                    "under_construction": "En Construcción",
                    "contact_part1": "¿Necesitas contactarme?",
                    "contact_part2": "Puedes hacerlo vía"
                }
            }
        }
    });
    updateLangstrings();
    i18next.on('languageChanged', () => {
        updateLangstrings();
    });
    // Language button init
    document.getElementById("language-toggler").addEventListener("click", () => {
        if (i18next.language === "en") {
            i18next.changeLanguage("es");
        } else {
            i18next.changeLanguage("en");
        }
    });
});