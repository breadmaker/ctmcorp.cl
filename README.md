# Sitio de CTM Corp. #

Este es el código fuente del sitio http://ctmcorp.cl/, el cual
está disponible para cualquiera, en caso que desee saber como fue construido el
sitio. El código fuente ha sido liberado, en concordancia con mis principios,
bajo los términos de la licencia [GPLv3](http://www.gnu.org/copyleft/gpl.html) o
superior.

## Acerca del desarrollo ##

Usando las potentes capacidades de [NPM](https://www.npmjs.com/), el gestor de
paquetes de Node.js, es posible no solo instalar/actualizar, si no también
automatizar labores de construcción, lo que en proyectos de baja escala elimina
la necesidad de utilizar automatizadores como Grunt o Gulp.

En este caso en particular, utilizo NPM para ayudarme en la verificación del
javascript (lint), y en la minificación del HTML, el Javascript y el CSS.

### Instalando dependencias ###

Entonces, para construir las modificaciones hechas, se debe preparar el sistema
instalando `nodejs` y `npm`. Esto se hace sólo la primera vez (se puede
verificar si es que ya se está preparado intentando ejecutar el comando `npm`).
En un sistema de tipo Debian, los comandos serían:

```bash
$ sudo apt-get install nodejs npm nodejs-legacy
$ sudo npm update -g # [OPCIONAL] Actualiza npm y dependencias a la última versión
```

Luego se proceden a instalar las dependencias del proyecto, ejecutando el
siguiente comando en la carpeta del mismo:

```bash
$ cd /ruta/hacia/ctmcorp.cl
$ npm install
```

Cada vez que se modifique alguna parte del sitio, en rigor, el contenido de
`src`, debe ejecutarse el script de construcción, ejecutando:

```bash
$ cd /ruta/hacia/ctmcorp.cl
$ npm run build
```

También es posible automatizar tal proceso, ejecutando:

```bash
$ cd /ruta/hacia/ctmcorp.cl
$ npm run build:watch
```

Y eso es todo.

## ¿Dudas? ##

Puedes abrir una
[nueva incidencia](https://gitlab.com/breadmaker/ctmcorp.cl/issues/new),
indicando la consulta o problema que tengas. Intentaré responder lo más pronto
posible. *Happy hacking!*
